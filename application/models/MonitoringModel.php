<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class MonitoringModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getMonitorings() {		
		$this->db->select('*');
		$this->db->from('monitoring');
		$this->db->join('product', 'product.product_id = monitoring.monitoring_id');
		$this->db->join('market', 'market.market_id = monitoring.market_id');
		$this->db->join('store', 'store.store_id = monitoring.store_id');
		$query = $this->db->get();
		return $query->result();
	}

	public function getMonitoring($id) {
		$this->db->select('*');
		$this->db->from('monitoring');
		$this->db->join('product', 'product.product_id = monitoring.monitoring_id');
		$this->db->join('market', 'market.market_id = monitoring.market_id');
		$this->db->where('monitoring.monitoring_id', $id); 
		$query = $this->db->get();
		return $query->result();
	}

	public function getMonitoringByMarket($market_id) {
		$this->db->select('*');
		$this->db->from('monitoring');
		$this->db->join('product', 'product.product_id = monitoring.monitoring_id');
		$this->db->join('market', 'market.market_id = monitoring.market_id');
		$this->db->where('monitoring.market_id', $market_id); 
		$query = $this->db->get();
		return $query->result();

	}
	public function getMonitoringByMarketByDate($market_id, $date) {
		$this->db->select('*');
		$this->db->from('monitoring');
		$this->db->join('product', 'product.product_id = monitoring.monitoring_id');
		$this->db->join('market', 'market.market_id = monitoring.market_id');
		$this->db->where('monitoring.market_id', $market_id);
		$this->db->where('monitoring.date', $date);
		$query = $this->db->get();
		return $query->result();
	}

	public function getAllDates() {
		$sql = "SELECT DATE(monitoring_date) AS mon_date FROM monitoring GROUP BY mon_date";
		$query = $this->db->query($sql);
		return $query->result();
	}

	public function getReportDate($date) {
		$sql = "SELECT monitoring.*, market.`market_name`, product.`product_name` FROM monitoring 
					LEFT JOIN product ON product.product_id = monitoring.`product_id`
						LEFT JOIN market ON market.market_id = monitoring.`market_id`
				 WHERE DATE(monitoring_date) ='". $date . "'";
		$query = $this->db->query($sql);
		return $query->result();

	}

}