<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class TrendModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getTrend() { 
		$this->db->select('*');
		$this->db->from('monitoring');
		$this->db->join('product', 'product.product_id = monitoring.product_id');
		$query = $this->db->get();
		return $query->result();

	}

	public function getProduct(){
			$query= $this->db->get('product');
			return $query->result();

		}

	public function getCommodityTrend($product_id){
		$query = $this->db->get_where('monitoring', ['product_id' => $product_id]);
		return $query->result();
	}

	public function getTrendByProduct($id) {
		$this->db->where('product_id', $id);
		$query = $this->db->get('summary_prev_price');
		$result = $query->result();
		return $result;
	}
 
	public function getTrendDateRange($start, $end, $prod_id) {
		// send separate for cogon and carment
		$data['cogon'] = $this->getPrevPriceCogon($start, $end, $prod_id);
		$data['carmen'] = $this->getPrevPriceCarmen($start, $end, $prod_id);
		return $data;
	}

	public function getPrevPriceCogon($start, $end, $prod_id) {
		// get all prev price of product from cogon
		// filter through range of date
		$this->db->where('product_id', $prod_id);
		$query = $this->db->get('prev_price_cogon');
		return $query->result();
	}
	
	public function getPrevPriceCarmen($start, $end, $prod_id) {
		// get all prev price of product from carmen
		// filter through range of date
		$this->db->where('product_id', $prod_id);
		$query = $this->db->get('prev_price_carmen');
		return $query->result();
	}

	public function getLatestPrevPrice($prod_id) {
		// get prev price both from cogon and carmen
		// filter by product
		// purpose is to have default view for report page when single product is clicked

        $sql = "SELECT * FROM agritrend.summary_prev_price WHERE product_id ='". $prod_id ."' ORDER BY monitoring_date DESC LIMIT 1";
        $query = $this->db->query($sql);
        $result = $query->result();
        if ($result) {
            return $result;
        }
        return false;
    }

    public function getLatestPrevPriceAll() {
    	$sql = "SELECT * FROM agritrend.summary_prev_price";
    	$query = $this->db->get('agritrend.summary_prev_price');
    	$result = $query->result();
    	if ($result) {
    		return $result;
    	}
    	else {
    		return false;
    	}
    }
 }