<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class ProductModel extends CI_Model 
{
	function __contruct() {
		parent::__contruct();
	}

	public function getProducts() {		
		$this->db->select('product.product_id, product.product_name, category.category_id, category.category_name, product.description, product.date_created, product.unit');
		$this->db->from('product');
		$this->db->join('category', 'category.category_id = product.category_id', 'left');
		$this->db->order_by("product.product_name", "asc");
		$query = $this->db->get();
		return $query->result();
	}


	public function getProduct($id) {
		$this->db->select('product.product_id, product.product_name, category.category_id, category.category_name, product.description, product.date_created, product.unit');
		$this->db->from('product');
		$this->db->join('category', 'category.category_id = product.category_id', 'left');
		$this->db->order_by("product.product_name", "asc");
		$this->db->where('product.product_id', $id); 
		$query = $this->db->get();
		return $query->result();
	}

	//  para makuha ang mga products under ani na category
	public function getProductByCategory($id) {
		$this->db->select('product.product_id, product.product_name, product.category_id, category.category_name');
		$this->db->from('product');
		$this->db->join('category', 'category.category_id = product.category_id', 'left');
		$this->db->order_by("product.product_name", "asc");
		$this->db->where('product.category_id', $id); 
		$query = $this->db->get();
		return $query->result();
	}

	public function getProductUnit() {		
        $sql = "SELECT product_id, unit FROM product GROUP BY product_id, unit";
        $query = $this->db->query($sql);
        return $query->result();
	}

	public function addNewProduct($product_info) {
		$this->db->insert('product', $product_info);
		return $this->db->insert_id();
	}

	public function updateProduct($info) {
	    $data = array(
	    	'description' => $info['product_description'],
			'product_name' => $info['product_name'],
			'category_id' => $info['category_id'],
	    	'date_updated' => date("Y-m-d")
	    );
	    $this->db->where('product_id', $info['product_id']);
	    $this->db->update('product', $data);
		return $this->db->affected_rows();
	}
}