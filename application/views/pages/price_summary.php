<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('template/header');
 ?>

<body>

    <div>
        <h1>Price Reports</h1>

        <table id="book-table">
            <thead>
                <tr>
                    <td>Book Title</td>
                    <td>Book Price</td>
                    <td>Book Author</td>
                    <td>Rating</td>
                    <td>Publisher</td>
                </tr>
            </thead>
            <tbody>
                
            </tbody>
        </table>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {
            $('#price-table').DataTable();
        });
    </script>

</body>
</html>
