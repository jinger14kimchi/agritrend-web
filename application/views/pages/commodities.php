<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('template/header');
 ?>

<body>

<div class="wrapper">
    <?php $this->load->view('pages/sidebar'); ?>

    <div class="main-panel">
        <?php $this->load->view('pages/navigation'); ?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <?php if(isset($message)) { ?>
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="card">
                                                <h2><?php echo $message; ?></h2>
                                            </div>
                                        </div>
                                    </div>
                                <?php }  ?>

                                <button type="button" class="btn btn-primary btn-md pull-right" data-toggle="modal" data-target="#myModal">
                                  Add New
                                </button>
                                <button type="button" class="btn btn-primary btn-md pull-right" data-toggle="modal" data-target="#addCategoryModal">
                                  New Category
                                </button>
                                <h4 class="title">List of Commodities</h4>
                                <p class="category">Agents monitor market prices</p>                               
                                  
                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                        <th>Commodity</th>
                                    	<th>Category</th>
                                        <th>Description</th>
                                        <th>Date Added</th>
                                        <th>Action</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($commodities as $com) { ?>   
                                        <tr id="<?php echo $com->product_id ?>" data-toggle="modal" data-target="#editModal">                                     
                                            <td class="prod_id"><?php echo $com->product_id ?></td>                                       
                                            <td class="prod_name"><?php echo $com->product_name ?></td>                                     
                                            <td class="cat_name"><?php echo $com->category_name ?></td>                                    
                                            <td class="prod_desc"><?php echo $com->description ?></td>                                  
                                            <td class="prod_date">
                                                <?php $d = strtotime($com->date_created); 
                                                    echo date('M d, Y', $d); ?>
                                            </td>                                
                                            <td>
                                                Edit                                      
                                            </td>
                                        </tr>
                                       <?php } ?>
                                       
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
             <a  href="<?= base_url('pages/report/generate');?>"  class="btn btn-secondary">Print PDF</a>

            <a  href="<?= base_url('pages/excel_export');?>"  class="btn btn-secondary">Save as Excel</a>
        </div>
<?php
    $this->load->view('template/footer');
 ?>


    </div>
</div>
 
<!-- Modal Add Category -->

<div class="modal fade" id="addCategoryModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Category</h4>
      </div>
        <form action="<?php echo base_url('/pages/commodities/addcategory'); ?>" method="POST">
              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Category Name</label><sup class="required-field"> *field required</sup>
                            <input type="text" class="form-control" name="category_name" placeholder="Name" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea type="email" class="form-control" name="category_description" placeholder="Add addtional information about this commodity(optional)"></textarea>
                        </div>
                    </div>
                </div>
           
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div> 
            <div class="clearfix"></div>
      </form>
        </div>
      </div>
    </div>

<!-- End Modal Add Category-->



<!-- Modal Add New -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Commodity</h4>
      </div>
        <form action="<?php echo base_url('/pages/commodities/addnew'); ?>" method="POST">
              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Choose category </label><sup class="required-field"> *field required</sup><br>
                                <select class="btn-group select" name="category_id" required>
                                    <option>Select Category</option>
                                 <?php foreach ($categories as $cat) { ?>
                                    <option value="<?php echo $cat->category_id ?>"><?php echo $cat->category_name ?></option>
                               <?php  } ?>
                                </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Name of Commodity </label><sup class="required-field"> *field required</sup>
                            <input type="text" class="form-control" name="product_name" placeholder="Name" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea type="email" class="form-control" name="description" placeholder="Add addtional information about this commodity(optional)"></textarea>
                        </div>
                    </div>
                </div>
           
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div> 
            <div class="clearfix"></div>
      </form>
        </div>
      </div>
    </div>

<!-- End Modal Add new -->


<!-- Modal PRODUCT New -->

<div class="modal fade" id="addProduct" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Commodity</h4>
      </div>
        <form action="<?php echo base_url('/pages/commodities/addnew'); ?>" method="POST">
              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Choose category </label><sup class="required-field"> *field required</sup><br>
                                <select class="btn-group select" name="category_id" required>
                                    <option>Select Category</option>
                                 <?php foreach ($categories as $cat) { ?>
                                    <option value="<?php echo $cat->category_id ?>"><?php echo $cat->category_name ?></option>
                               <?php  } ?>
                                </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Name of Commodity </label><sup class="required-field"> *field required</sup>
                            <input type="text" class="form-control" name="product_name" placeholder="Name" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea type="email" class="form-control" name="description" placeholder="Add addtional information about this commodity(optional)"></textarea>
                        </div>
                    </div>
                </div>
           
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div> 
            <div class="clearfix"></div>
      </form>
        </div>
      </div>
    </div>

<!-- End Modal Add new -->


<!-- Modal Edit  -->

<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Edit Details</h4>
      </div>
        <form action="<?php echo base_url('/pages/commodities/updateproduct'); ?>" method="POST">
            <div class="modal-body">      
                <div class="row">   
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Product ID</label><sup class="required-field"></sup>
                            <input type="text" class="form-control" id="edit-prod-id" name="product_id">
                        </div>
                    </div>
                </div>    
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Category</label><sup class="required-field"> *field required</sup><br>
                                <select class="btn-group select" name="category_id" id="edit-category" required>
                                 <?php foreach ($categories as $cat) { ?>
                                    <option value="<?php echo $cat->category_id ?>"><?php echo $cat->category_name ?></option>
                               <?php  } ?>
                                </select>
                        </div>
                    </div>
                </div>          
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Name</label><sup class="required-field"> *field required</sup>
                            <input type="text" class="form-control" id="edit-prod-name" name="product_name" placeholder="Name" required>
                        </div>
                    </div>
                </div>              
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Description</label>
                            <input type="text" class="form-control" id="edit-desc" name="product_description" placeholder="Name" required>
                        </div>
                    </div>
                </div>

            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
            </div> 
            <div class="clearfix"></div>
        </form>
        </div>
      </div>
    </div>

<!-- End Modal Edit -->



<script type="text/javascript">
   $('#editModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget) 
        let recipient = button.data('whatever') 
        let id = recipient;
        $.ajax({    
            type: "GET",
            url: "http://localhost/agritrend-web/api/product/product",
            data: {id:id},
            dataType: "json",               
            success: function(res){  
                let data = res[0];
                $('#edit-prod-name').val(data.product_name);
                $('#edit-desc').val(data.description);
                $('#edit-category').val(data.category_name);
                $('#edit-prod-id').val(data.product_id);
                $('select option[value='+data.category_id+']').attr("selected",true);
            }
        });
    })
</script>

</body>
</html>