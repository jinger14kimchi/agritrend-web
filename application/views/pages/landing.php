<!DOCTYPE html>
<html>
<head>
	<title>Admin</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>

<style type="text/css">
	.container{	display: block;	margin:auto; text-align: center; padding-top: 100px; }		
	img{width: 100%;}
	#logo{width: 35%; display:block; margin:auto;}
	#inputBtn{ margin-top: 15px; }
	.carousel { z-index: -999; } /* keeps this behind all content */
	.carousel .item { position: fixed; width: 100%; height: 100%; opacity: 	0.5;  transition: 
  		}
 

</style>
<body>
	<div id="myCarousel" class="carousel slide" data-ride="carousel">
		<!-- Indicators -->
		<ol class="carousel-indicators">
			<li data-target="#myCarousel" data-slide-to="0" class="active"></li>
			<li data-target="#myCarousel" data-slide-to="1"></li>
			<li data-target="#myCarousel" data-slide-to="2"></li>
		</ol>
		<!-- Wrapper for slides -->
		<div class="carousel-inner">
			<div class="item active">
				<img src="<?php echo base_url( '/assets/images/d.jpg');?>"  />
			</div>
			<div class="item">
				<img src="<?php echo base_url( '/assets/images/f.jpg');?>"  />
			</div>
			<div class="item">
				<img src="<?php echo base_url( '/assets/images/b.jpg');?>"  />
			</div>
			<div class="item">
				<img src="<?php echo base_url( '/assets/images/c.jpg');?>"  />
			</div>
			<div class="item">
				<img src="<?php echo base_url( '/assets/images/a.jpg');?>"  />
			</div>
		</div>		
	</div>
	<div class="container center-block">
		<img src="<?php echo base_url('assets/images/agrilogo.png');?>" id="logo" >
		
			<a  href="<?= base_url('pages/login');?>"  class="btn btn-success btn-lg" id="inputBtn">Login</a>
	</div>	
</body>
</html>