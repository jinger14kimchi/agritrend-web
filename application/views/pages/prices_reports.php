<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('template/header');
 ?>
<body>

<div class="wrapper">
    <?php $this->load->view('pages/sidebar'); ?>

    <div class="main-panel">
        <?php $this->load->view('pages/navigation'); ?>

        <div class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Price Reports</h4>   

                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped">
                                    <thead>
                                        <th>ID</th>
                                    	<th>Category </th>
                                    	<th>Commodity </th>
                                    	<th>Price</th>
                                        <th>Market</th>
                                        <th>Monitoring Date</th>
                                        <th>Agent ID</th>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($reports->result as $report) { ?>   
                                        <tr id="<?php echo $report->monitoring_id ?>">                                     
                                            <td class=""><?php echo $report->monitoring_id ?></td>                 
                                            <td class=""><?php echo $report->category_name ?></td>                                        
                                            <td class=""><?php echo $report->product_name ?></td>                                 
                                            <td class=""><?php echo $report->price ?></td>                                    
                                            <td class=""><?php echo $report->market_name ?></td>                                           
                                            <td class=""><?php echo (new DateTime($report->monitoring_date))->format('Y-m-d'); ?></td>                                   
                                            <td class=""><?php echo $report->user_id ?></td>                                   
                                          
                                        </tr>
                                       <?php } ?>
                                       
                                    </tbody>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
	<?php $this->load->view('template/footer');  ?>


    </div>
    </div>
</div>



<script type="text/javascript">
   $('#editUserModal').on('show.bs.modal', function (event) {
        let button = $(event.relatedTarget) 
        let recipient = button.data('whatever') 
        let id = recipient;
        $.ajax({    
            type: "GET",
            url: "http://localhost/agritrend-web/api/users/users",
            data: {id:id},
            dataType: "json",               
            success: function(res){  
                let data = res[0];
                console.log(data);
                $('#edit-user-id').val(data.user_id);
                $('#edit-username').val(data.username);
                $('#edit-fname').val(data.fname);
                $('#edit-lname').val(data.lname);
                $('#edit-birthdate').val(data.birthdate);
                console.log(data.gender);
                $('select#gender option[value='+data.gender+']').attr("selected",true);            
            }
        });
    })
</script>

</body>
</html>