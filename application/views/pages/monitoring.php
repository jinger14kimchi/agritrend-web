<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('template/header');
 ?>

<body>

<div class="wrapper">
    <?php  $this->load->view('pages/sidebar'); ?>

    <div class="main-panel">
        <?php $this->load->view('pages/navigation'); ?>

        <div class="content">
            <div class="container-fluid">
                
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Filter Reports</h4>
                            </div>
                            <div class="content">                                                            
                                    
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Filter Category</label><br>
                                                <select name="" id="category">
                                                    <option value="">Choose Category</option>
                                                    <?php foreach ($categories as $cat) {?>
                                                        <option value="<?= $cat->category_name?>"><?= $cat->category_name ?></option>

                                                    <?php }?>
                                                </select>
                                            </div>
                                            <div class="col-md-4">
                                                <label for="">Filter Commodity</label><br>
                                                <select name="" id="commodity">
                                                    <option value="">Choose Commodity</option>
                                                    <?php foreach ($products as $prod) {?>
                                                        <option value="<?= $prod->product_name?>" id=""><?= $prod->product_name ?></option>

                                                    <?php }?>
                                                </select>
                                            </div>
                                        </div>

                            </div>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Price Reports</h4>   

                            </div>
                            <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="table">
                                    <thead>
                                    	<th>Category </th>
                                    	<th>Commodity </th>
                                    	<th>Unit </th>
                                    	<th>COGON MARKET<br>Prevailing Price</th>
                                        <th>CARMEN MARKET<br>Prevailing Price</th>
                                        <th>Monitoring Date</th>
                                        <th>Sources</th>
                                    </thead>
                                    <tbody>
                                         <?php foreach ($reports as $report) { ?>
                                      <tr id="<?php echo $report->product_id ?>" >                                                                   

                                           <td class=""><a href="#" > <?php echo $report->category_name ?></a></td>

                                            <td class=""><a href="<?= base_url('pages/productHistory/history?id=') . $report->product_id  ;?> " ><?php echo $report->product_name ?></a></td>   
                                            <td class=""><?php echo $report->unit ?></td>                                 
                                            <td class=""><?php echo $report->cogon_prev_price ?: "N/A" ?></td>                     
                                            <td class=""><?php echo $report->carmen_prev_price ?: "N/A" ?></td>  
                                            <td class="">                                                
                                                <?php $d = strtotime($report->monitoring_date); 
                                                    echo date('M d, Y', $d); ?> 
                                            </td>                     
                                            <td class=""><?php echo $report->cogon_source . ';' . $report->carmen_source ?></td>                                     
                                          
                                        </tr>
                                         <?php } ?> 
                                    </tbody>
                                    <tfoot></tfoot>
                                </table>

                            </div>
                        </div>
                    </div>

                </div>

            </div>
	<?php $this->load->view('template/footer');  ?>


    </div>
    </div>
</div>




</body>

 
<script type="text/javascript">
$(document).ready(function (){
    $.noConflict();
    var table = $('#table').DataTable({
       dom: 'lrtip'
    });
    
    $('#category').on('change', function(){
       table.search(this.value).draw();   
    });

    $('#commodity').on('change', function(){
       table.search(this.value).draw();   
    });
});
 



    // $('#btn-product').on('click', function() {
    //     let id = $('#prod_id').val();
    //     $.ajax({
    //         method: "GET",
    //         url: "http://localhost/agritrend-web/api/prices/product",
    //         data: {
    //             product_id: id
    //         },
    //         success: function(response) {
    //             console.log(response)
    //             $('#result').text(response[0].product_name)
    //         },
    //         error: function (request) {
    //             $('#result').text(request.responseText)
    //         }
    //     });
    // });
</script>
</html>