<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('template/header');
 ?>


 


<body>

<div class="wrapper">
    <?php  $this->load->view('pages/sidebar'); ?>

    <div class="main-panel">
        <?php $this->load->view('pages/navigation'); ?>

        <div class="content">
            <div class="container-fluid">
                
            <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                <h4 class="title">Product History</h4>
                            </div>
                            <div class="content">                                                            
                                    
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label for="">Filter Category</label><br>
                                                <select name="" id="category">
                                                    <option value="">Choose Month</option>
                                                        <p> <mi class="pe-7s-arrow-left"></i></p>
                                                    <?php foreach ($categories as $cat) {?>
                                                        <option value="<?= $cat->category_name?>"><?= $cat->category_name ?></option>

                                                    <?php }?>
                                                </select>
                                            </div>
                                          
                                        </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="header">
                                 <a href=" <?=base_url('pages/price/reports')?>"> <i class="pe-7s-left-arrow" style="font-size: 30px; color: green"></i></a>
                                <h4 class="title" value="<??>" name="product"><?php echo($product[0]->product_name);?></h4>  
                                <h5> Category: <?php echo($reports[0]->category_name); ?></h5>

  <div class="content table-responsive table-full-width">
                                <table class="table table-hover table-striped" id="table">
                                    <thead>
        
                                        <th>DATE</th>
                                        <th>COGON MARKET <br> PREVAILING PRICE</th>
                                        <th>CARMEN MARKET <br> PREVAILING PRICE</th></th>
                                        <th>UNIT</th>

                                    </thead>
                                    <tbody>
                                          <?php foreach ($reports as $report) { ?>
                                      <tr id="<?php echo $report->product_id ?>" >                              

                                               <td class="">                                                
                                                <?php $d = strtotime($report->monitoring_date); 
                                                    echo date('M d, Y', $d); ?> 
                                            </td>                           
                                            <td class=""><?php echo $report->cogon_prev_price ?></td>                     
                                            <td class=""><?php echo $report->carmen_prev_price ?></td>  
                                                                        
                                             <td class=""><?php echo $report->unit ?></td>             
                                          
                                        </tr>
                                         <?php } ?> 
                                       
                                    </tbody>
                                </table>

                            </div>
                            </div>
                        </div>
                    </div>
                </div>
          	<?php $this->load->view('template/footer');  ?>


    </div>
    </div>
</div>
</div>	




</body>

 
<script type="text/javascript">
// $(document).ready(function (){
//     $.noConflict();
//     var table = $('#table').DataTable({
//        dom: 'lrtip'
//     });
    
//     $('#category').on('change', function(){
//        table.search(this.value).draw();   
//     });

//     $('#commodity').on('change', function(){
//        table.search(this.value).draw();   
//     });
// });
 



    // $('#btn-product').on('click', function() {
    //     let id = $('#prod_id').val();
    //     $.ajax({
    //         method: "GET",
    //         url: "http://localhost/agritrend-web/api/prices/product",
    //         data: {
    //             product_id: id
    //         },
    //         success: function(response) {
    //             console.log(response)
    //             $('#result').text(response[0].product_name)
    //         },
    //         error: function (request) {
    //             $('#result').text(request.responseText)
    //         }
    //     });
    // });
</script>
</html>