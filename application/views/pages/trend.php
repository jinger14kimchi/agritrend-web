<?php
    defined('BASEPATH') OR exit('No direct script access allowed');
    $this->load->view('template/header');
 ?>
 
<body>

<div class="wrapper">
    <?php  $this->load->view('pages/sidebar'); ?>

    <div class="main-panel">
        <?php $this->load->view('pages/navigation'); ?>

        <div class="content">
            <div class="container-fluid">
                
								<div class="row">
										<div class="col-md-12">
												<div class="col-md-3">
													<div class="card">
														<label for="">Choose Graph</label><br>
														<select id="graph_type" onchange="changeType()">
															<option selected="true" value="">Choose Graph</option>
															<option value='line' selected="selected">Line</option>
															<option value='bar'>Bar</option>
														</select>					
													</div>	
												</div>
												

													<div class="col-md-3">
														<div class="card">
																<label for="">Choose Category</label><br>
																	<select name="" id="category" onchange="getProductsByCategory()">
																		<option value="">Select Category</option>
																		<?php foreach ($categories as $cat) {?>																				
																				<option value="<?php echo $cat->category_id?>" id=""><?= $cat->category_name ?></option>
																		<?php }?>
																</select>
															</div>	
													</div>

													<div class="col-md-3">
														<div class="card">
																<label for="">Choose Commodity</label><br>
																	<select name="" id="commodity" onchange="getCommodityTrend()">	
																		<option value="">Select Commodity</option>																	
																	</select>
															</div>	
													</div>
													<div class="col-md-3">
														<div class="card">
														<label for="">Start Date</label><br>
														<input 
																type="date" 
																id="start" 
																name="start-date"
																min="2013-01-01" >
															
													</div>	
												</div>
													<div class="col-md-3">
														<div class="card">
														<label for="">End Date</label><br>
														<input 
															type="date" 
															id="end" 
															name="end-date" >															
													</div>	
												</div>
								</div>

								<button onclick="showChart()">
									Show Graph
								</button>
								<!-- row end -->

								<!-- Chart  -->
								<div class="row">
									<div class="col-md-12">
										<div class="card">
												<div class="container-fluid">
													<canvas id="myChart"></canvas>
												</div>
										</div>
									</div>
								</div>
								<!-- End Chart -->
						</div> 
						<!-- container fluid -->
				</div>  
				<!-- content  -->
			<?php $this->load->view('template/footer');  ?>
	</div>
</div>




<!-- SCRIPT  FROM ANGELY --> 
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.4.0/Chart.min.js"></script>
	

  <script type="text/javascript">
      var type = 'bar';
      var prod = [];
      var categoryList = [];
			var graph_type = document.getElementById('graph_type');
      // getPro();

      function changeType() {
				graph_type = document.getElementById('graph_type'); 
        type = graph_type.value;
        showChart(type);
      }

      function showChart(graph_type = 'line') {
	        var ctx = document.getElementById('myChart').getContext('2d');
	        getCommodityTrend();
            var label = [];
            for (var i = 0; i<prod.length; i++) {
	        	label.push(prod[i]['monitoring_date']);
			}
            var chart = new Chart(ctx, {
              type: graph_type,
              data: {
					labels: label,    
					fill: false,                      
	                  	datasets: [{
							label: label,
							backgroundColor: 'rgb(0, 230, 118)',
							borderColor: 'rgb(255, 99, 132)',
							data: [0, 10, 5, 2, 20, 30, 45],
		                }]
	              },
	              options: {}
              });
	        }

		function getCommodityTrend() {
            $(document).ready(function (){
			let prod_id = $('#commodity').val();
              $.ajax({    
                  type: "GET",
                  url: "http://localhost/agritrend-web/api/trends/trend_product?id="+prod_id,
                  dataType: "json",               
                  success: function(response){ 
                    prod = response;
                    showChart();
                  }
              });
            });
		}

        function getPro() {
					//  get all commodities to show in dropdown
            $(document).ready(function (){
              $.ajax({    
                  type: "GET",
                  url: "http://localhost/agritrend-web/api/product/product",
                  dataType: "json",               
                  success: function(response){  
                    console.log(response);
                    prod = response;
                    showChart();
                  }
              });
            })
        }

        function getProductsByCategory() {
            $(document).ready(function (){
				var cat_id = $('#category').val();
				console.log('cat-id ', cat_id);
	              $.ajax({    
	                  type: "GET",
	                  url: "http://localhost/agritrend-web/api/product/category?id="+cat_id,
	                  dataType: "json",               
	                  success: function(response){  
	                    if (response) {
		                    prod = response;
	                    	changeProductShow(prod);
	                    	showChart();
	                    }
	                  },
					  error: function(XMLHttpRequest, textStatus, errorThrown) {
					     console.log('error', textStatus);
					     changeProductShow(null);
					  }
	              });
            })
        }

        function changeProductShow(product) {
        	if (product != null) {
	            $(document).ready(function (){
	            	var html = "";
					for (var i = 0; i < product.length; i++) {
						let p = product[i];
						let id = p.product_id
						let pName = p.product_name;
						console.log(id, pName);
						html += '<option value="'+ id +'">'+ pName +'</option>';
					}
					console.log(html)
					$('#commodity').html(html);
	            });
        	} 
        	else {
        		$(document).ready(function () {
					var html = '<option value="Not Available"></option>';
					console.log(html);
					$('#commodity').html(html);
        		});
        	}

        }
      </script>

<!-- SCRIPTS END ANGELY -->
</body>
</html>