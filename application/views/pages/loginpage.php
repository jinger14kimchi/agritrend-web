<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<style type="text/css">
	.container{
		text-align:center;
		margin-left: auto;
		margin-right: auto;
		padding-top	top:15%;
		display: block;
		background-color: #00e676;
		border-radius: 50px 10px;
		width: 	500px;		
		padding-bottom: 20px;
		padding-top: 20px;
		margin-top: 100px;		
		position: relative;
	}

	.alertMess{
		text-align:center;
		left:40%;
		top: 30px;
		width: 	300px;	
		position: absolute;
		z-index: 1;	
	
	}

	#forgotPass{
		text-decoration: underline;
		display: block;
		color: #51555b;
		font-size: 18px;
		margin-top: 10px;
	}
	#forgotPass:hover{color:gray;}
	.btn{
		margin-top: 40px;
		display: block;
		background-color: white ;
	}
	h6{text-align: left; margin-left: 40px; font-size: 20px;}
	.input-lg{width: 400px; text-align: center; border:0;}
</style>
<body>

	<form class="login" method="POST" action="<?= base_url('pages/verify'); ?>">
	<div class="alertMess"><?php 
			if(isset($msg)){
		?>
			<div class="text-center alert alert-danger alert-dismissable">
				<a href="#" data-dismiss="alert" class="close">&times;</a>
				<?php 
					echo $msg;
				?>
			</div>
		<?php 
			}
		?>

	</div>
	<div class="container center-block">

		<h6>Username</h6>
		<input type="text" name="username" class="input-lg">
		<h6>Password</h6>
		<input type="password" name="password" class="	input-lg">
		<input type="submit" name="login" value="Login" class="btn  btn-lg center-block" >

	</div>
		
</form>
</body>
</html>