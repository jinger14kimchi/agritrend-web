
<!-- Modal Add New -->

<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Add New Commodity</h4>
      </div>
        <form action="<?php echo base_url('/pages/commodities/addnew'); ?>" method="POST">
              <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Choose category </label><sup class="required-field"> *field required</sup><br>
                                <select class="btn-group select" name="category_id" required>
                                    <option>Select Category</option>
                                 <?php foreach ($categories as $cat) { ?>
                                    <option value="<?php echo $cat->category_id ?>"><?php echo $cat->category_name ?></option>
                               <?php  } ?>
                                </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Name of Commodity </label><sup class="required-field"> *field required</sup>
                            <input type="text" class="form-control" name="product_name" placeholder="Name" required>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label for="exampleInputEmail1">Description</label>
                            <textarea type="email" class="form-control" name="description" placeholder="Add addtional information about this commodity(optional)"></textarea>
                        </div>
                    </div>
                </div>
           
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            <button type="submit" class="btn btn-primary">Save changes</button>
          </div> 
            <div class="clearfix"></div>
      </form>
        </div>
      </div>
    </div>

<!-- End Modal Add new -->
