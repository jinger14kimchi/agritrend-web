<?php

$obj_pdf = new TCPDF('P', PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
$obj_pdf->SetCreator(PDF_CREATOR);
/**$obj_pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, '', '');*/
$obj_pdf->setTitle($docTitle);
$obj_pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
$obj_pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));
$obj_pdf->SetDefaultMonospacedFont('helvetica');
$obj_pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
$obj_pdf->SetFooterMargin(PDF_MARGIN_FOOTER);
$obj_pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_RIGHT);
$obj_pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);
$obj_pdf->SetFont('helvetica', '', 9);
$obj_pdf->setFontSubsetting(false);
$obj_pdf->AddPage();
ob_start();


echo '<html>
			<body>
              <div> <h3 style="text-align:center" >Department of Agriculture - Regional Field Office 10</h3>
               <h3 style="text-align:center; line-height:0;">Agribusiness and Marketing Assistance Division (AMAD)</h3>
               <h3 style="text-align:center">PRICE MONITORING REPORT</h3>
               </div>

                <div>
                  <h4>Market : Cogon and Carmen</h4>
                  <h4 style="line-height: 0">Location : Cagayan de Oro City</h4>
                  <h4>Collection Date: '.$reports[0]->monitoring_date.'</h4>
                
                  <h4>Region : 10</h4>
                  <h4 style="line-height:0">Informant:</h4>
                  <h4>Office'.$reports[0]->monitoring_date.'</h4>
                </div>
                <table border="0.5" >
              
                    <thead >
                        <tr>
                            <th style="text-align: center;" >COMMODITY</th>
                            <th style="text-align: center;">UNIT</th>
                            <th style="text-align: center;">COGON<br>Prevailing Price</th>
                             <th style="text-align: center;">CARMEN<br>Prevailing Price</th>
                            <th style="text-align: center;">SOURCE</th>
                        </tr>
                    </thead>
                    <tbody>';
                            if(count($reports) > 0){
                                foreach($reports as $key){
                                    echo '<tr >
                                            <td style="text-align: center;" > '.$key->product_name .'</td>
                                            <td style="margin-left: !important">'.$key->unit.'</td>
                                            <td>'.$key->cogon_prev_price.'</td>
                                            <td>'.$key->carmen_prev_price.'</td>
                                       
                                          </tr>';
                                }
                            }
echo                '</tbody>
                </table>
                <div style="margin-top:100px">
                Submitted by:

                Noted by:
                </div>
            </body>
      </html>';

$content = ob_get_contents();
ob_end_clean();
$obj_pdf->writeHTML($content, true, false, true, false, '');
$fileName = $docTitle.'.pdf';
$obj_pdf->Output($fileName, 'I');