<?php

use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';


class Account extends REST_Controller 
{
    function __construct() {
        parent::__construct();
        $this->load->model('UsersModel', 'usersmodel');
    }

    public function signup_post() {
        $params = array(
            'username' => $this->post('username'),
            'password' => password_hash($this->post('password'), PASSWORD_BCRYPT),
            'gender' => $this->post('gender'),
            'birthdate' => $this->post('birthdate'),
            'user_type' => $this->post('user_type'),
            'fname' => $this->post('fname'),
            'lname' => $this->post('lname')
        );
        $last_id = $this->usersmodel->addUser($params);
        if ($last_id !== null) {
            $this->response( [
                'message' => 'Successfully signed up.'
            ], 200);
        }
        else {
            $this->response([
                'message' => "Error signing up. Please try again"
            ], 200);
        }
    }

    public function login_post() {
        $username = $this->post('username');
        $password = $this->post('password');

        $user = $this->usersmodel->validate($username, $password);

        if ($user) {
            $this->response([
                'message' => 'Successful Login', 
                'status' => true,
                'user' => $user
            ], 200);
        }
        else {
            $this->response([
                'message' => 'Invalid account.', 
                'status' => false
            ], 404);
        }
    }

    public function update_post() {        
		$info = $this->input->post();
		if ($this->usersmodel->updateUser($info)) {             
            $this->response([
                'message' => 'Successfully updated.'
            ], 200);
        }
        else {
            $this->response([
                'message' => 'Update error'
            ], 200);
        }
    }

}

