<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Verify extends CI_Controller {

    public function index() {
		$this->load->model('UsersModel', 'usermodel');
		$this->check();
	}

	public function check(){
        $username = $this->security->xss_clean($this->input->post('username'));
		$password = $this->security->xss_clean($this->input->post('password'));
		$check = $this->usermodel->validate($username, $password);
		if ($check == true) {
			$this->load->view('pages/dashboard');
		} 
		else {
			$data['msg'] = "Access Denied. Please check your account details and try again.";
			$this->load->view('pages/loginpage', $data);
		}
	}
}