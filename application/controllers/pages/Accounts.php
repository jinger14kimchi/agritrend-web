<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Accounts extends CI_Controller {

    function __construct() {
        parent::__construct();
		$this->load->model('UsersModel', 'usersmodel');
    }

	function index() {

	}

	public function agents() {
		$data['users'] = $this->usersmodel->getUserByType('agent');
		$data['type'] = 'Agent';
		if($this->check_access()){
			$this->load->view('pages/users', $data);
		}
	}

	public function users() {
		$data['users'] = $this->usersmodel->getUserByType('normal');
		$data['type'] = 'Mobile';
		if($this->check_access()){
			$this->load->view('pages/users', $data);
		}
	}
	public function admin() {
		$data['users'] = $this->usersmodel->getUserByType('admin');
		$data['type'] = 'Admin';
		if($this->check_access()){
			$this->load->view('pages/users', $data);
		}
	}



	public function check_access(){
		if ($this->session->userdata('validated') == true && $this->session->userdata('user_type') == 'admin'){
			return true;
		}
		else{
			$data['msg'] = "Access denied";
            $this->load->view('pages/loginpage', $data);
		}
	}
}