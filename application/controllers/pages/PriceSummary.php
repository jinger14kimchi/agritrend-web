<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class PriceSummary extends CI_Controller {
	public function index() {
		if($this->check_access()){        
				$this->load->helper('url');

				$this->load->view('pages/price_summary');
			}
	}
}